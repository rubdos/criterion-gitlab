# Gitlab + criterion.rs = :heart:

Goes together with [my blogpost](https://www.rubdos.be/linux/gitlab/automation/benchmarks/rust/2018/06/19/extreme-benchmark-feedback.html).

Don't forget to make a project variable `GITLAB_API_TOKEN` and set it to a fresh API token!
